## Prepare

- [ ] All "bugfix" issues for the next release done
- [ ] All "feature" issues for the next release done
- [ ] All those issues merged into the new version(-branch)

### Enhance

- [ ] Take care of one IDE warning
  - Group by severity and look into Error.PHP
  - Ignore Unused or Undefined (those are for major releases)
- [ ] Take care of typos

## Release

https://gitlab.com/rmp-up/wp-di/-/releases/new

- [ ] Rebase successful
- [ ] Bump year if needed
- [ ] Documentation
    - [ ] Tests/Documentation updated
    - [ ] Release contains change-log (`PAGER=cat git log --oneline 0.0.0..HEAD --no-merges --pretty=format:"* %s"`)
          since last release
    - [ ] Readme.md review/updated
- [ ] CI successful (no incomplete)

Release and have a nice day.
