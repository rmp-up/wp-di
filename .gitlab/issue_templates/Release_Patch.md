## Prepare

- [ ] All "bugfix" issues for the next release done
- [ ] All those issues merged into the new version(-branch)

### Enhance

- [ ] (Re-)Enable one more phpcs rule (compare phpcs-1.0.xml with phpcs.dist.xml)
  - Enable multiple rules and look at ...
  - `vendor/bin/phpcs --standard=etc/phpcs/phpcs.xml.dist -s --report=source lib/`
  - Fulfil one or more
  - Deactivate the rule if there is further work to do but keep the improvements
- [ ] Take care of one IDE warning
  - Group by severity and look into Error.PHP or Warning.PHP
  - Ignore Unused or Undefined (those are for major releases)
- [ ] Take care of typos

## Release

https://gitlab.com/rmp-up/wp-di/-/releases/new

- [ ] Rebase successful
- [ ] Bump year if needed
- [ ] Documentation
    - [ ] Tests/Documentation updated
    - [ ] Release contains change-log (`PAGER=cat git log --oneline 0.0.0..HEAD --no-merges --pretty=format:"* %s"`)
          since last release
    - [ ] Readme.md review/updated
- [ ] CI successful (no incomplete)

Release and have a nice day.
