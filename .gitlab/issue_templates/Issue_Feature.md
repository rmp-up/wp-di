<!-- Yeah! You found a way to improve the tool. -->
<!-- Please tell us everything about your idea. -->
<!-- Thanks in advance! -->

## Situation

<!-- How does your service definition look like? -->
```yaml

```

<!-- And please add some details if you like. -->
<!-- For example other plugins, themes or APIs. -->


## Problem

<!-- What was the thing that you did not expect to happen? -->
<!-- How hard did it affect your application? -->


## Solution

<!-- How would a solution look like? -->
```yaml

```

<!-- What do you expect to happen? -->
<!-- Are there other options/solutions? -->
<!-- Why is the given solution the best option? -->
