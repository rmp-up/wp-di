<!-- Oh no, a bug. How could this happen? -->
<!-- Please tell us about the basics and answer the following questions. -->
<!-- Thanks in advance! -->

## Situation

<!-- Which versions are you using? -->
PHP Version:

WordPress Version:

wp-di Version:


<!-- How does your service definition look like? -->
```yaml

```

<!-- And please add some details if you like. -->
<!-- For example other plugins, themes or APIs. -->


## Problem

<!-- What was the thing that you did not expect to happen? -->
<!-- How hard did it affect your application? -->


## Solution

<!-- How would a solution look like? -->
```yaml

```

<!-- What do you expect to happen? -->
<!-- Are there other options/solutions? -->
<!-- Why is the given solution the best option? -->
