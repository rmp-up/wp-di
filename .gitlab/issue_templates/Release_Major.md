## Prepare

- [ ] Open a new branch (e.g. "release/0.8")
- [ ] All "breaking changes" issues for the next release done
- [ ] All those issues merged into the new version(-branch)

### Deprecations

- [ ] Handle deprecations
    - [ ] Look for `@deprecated` and the word "deprecated" itself and clean up the code
    - [ ] Check IDE code inspections for unused code
    - [ ] In "phpstan.neon" set `reportUnmatchedIgnoredErrors: true` and look for deprecated ignore pattern
- [ ] Update as much of `composer outdated --direct` as possible (using the lowest PHP version)
- [ ] Previous Major Version:
    - [ ] Compare with next major and pull simple forward-compatible changes (e.g. docs, rename of variables, etc.).
      The more there are the less we paid attention to solve them in the current branch,
      which should be solved next time.
    - [ ] Compare and add `Deprecated::forwardCompatible( $message )` to all removed/changes interfaces.
      This shall warn developer about upcoming breaking changes.
    - [ ] Try solving them by creating a forward-compatible solution
      or make it a `Deprecated::forwardIncompatible($message)` warning.
    - [ ] Release a forward-compatibility (version marked with "-fc" as pre-release identifier)
      patch that shows E_USER_DEPRECATED for all deprecation in the logs.

### Enhance

- [ ] (Re-)Enable one more phpcs rule (compare phpcs-1.0.xml with phpcs.dist.xml)
  - Enable multiple rules and look at ...
  - `vendor/bin/phpcs --standard=etc/phpcs/phpcs.xml.dist -s --report=source lib/`
  - Fulfil one or more
  - Deactivate the rule if there is further work to do but keep the improvements
- [ ] Take care of one IDE warning
  - Group by severity and look into Error.PHP or Warning.PHP
  - Ignore Unused or Undefined (those are for major releases)
- [ ] Take care of typos

### Pre-Release

- [ ] Upcoming Version: Pre-Release a release candidate (version marked with "-rc") candidate
  and wait two weeks for bugs.
- [ ] Redo RC (release candidates) when all bugs gone and wait another 2 weeks.

## Release

https://gitlab.com/rmp-up/wp-di/-/releases/new

- [ ] Rebase successful
- [ ] Bump year if needed
- [ ] Documentation
    - [ ] Tests/Documentation updated
    - [ ] Release contains change-log (`PAGER=cat git log --oneline 0.0.0..HEAD --no-merges --pretty=format:"* %s"`)
          since last release
    - [ ] Readme.md review/updated
- [ ] CI successful (no incomplete)

Release and have a nice day.
