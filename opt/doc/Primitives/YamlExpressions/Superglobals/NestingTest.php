<?php
/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4: */

/**
 * NestingTest.php
 *
 * LICENSE: This source file is created by the company around M. Pretzlaw
 * located in Germany also known as rmp-up. All its contents are proprietary
 * and under german copyright law. Consider this file as closed source and/or
 * without the permission to reuse or modify its contents.
 * This license is available through the world-wide-web at the following URI:
 * https://rmp-up.de/license-generic.txt . If you did not receive a copy
 * of the license and are unable to obtain it through the web, please send a
 * note to mail@rmp-up.de so we can mail you a copy.
 *
 * @package   wp-di
 * @copyright 2021 Pretzlaw
 * @license   https://rmp-up.de/license-generic.txt
 */

declare( strict_types=1 );

namespace RmpUp\WpDi\Test\Primitives\YamlExpressions\Superglobals;

use RmpUp\WpDi\Test\AbstractTestCase;
use RmpUp\WpDi\Test\Mirror;
use SomeThing;

/**
 * Details about multi-dimensional arrays
 *
 * When a Superglobal is a multi-dimensional array
 * but you just want to use one part or primitive value then
 * provide the path to it.
 *
 * ```yaml
 * services:
 *   SomeThing:
 *     arguments:
 *       - !session [ nx01, engineer, first ]
 *       - !session [ nx01, maco ]
 * ```
 *
 * For an array like the following ...
 *
 * ```yaml
 * # $SESSION =
 * nx01:
 *   captain: Archer
 *   officer: T'Pol
 *   engineer:
 *     first: Tucker
 *     second: Kelby
 * ```
 *
 * ... it would inject the string "Tucker" as the first argument.
 *
 * But the second argument is injected as `null`
 * because it does not exist.
 * We do this instead of raising an exception,
 * to allow a more dynamical environment.
 *
 * @copyright 2021 Pretzlaw (https://rmp-up.de)
 */
class NestingTest extends AbstractTestCase
{
	/**
	 * @var array
	 */
	private $injectedArguments;

	protected function compatSetUp()
	{
		parent::compatSetUp();

		$_SESSION = $this->yaml(1);
		$this->registerServices();

		/** @var Mirror $mirror */
		$mirror = $this->pimple[SomeThing::class];
		$this->injectedArguments = $mirror->getConstructorArgs();
	}

	public function testDeepSessionValueIsInjected()
	{
		static::assertEquals('Tucker', $this->injectedArguments[0]);
	}

	public function testUndefinedNodeBecomesNull()
	{
		static::assertNull($this->injectedArguments[1]);
	}
}
