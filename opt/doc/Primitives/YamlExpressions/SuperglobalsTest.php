<?php
/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4: */

/**
 * SuperglobalsTest.php
 *
 * LICENSE: This source file is created by the company around M. Pretzlaw
 * located in Germany also known as rmp-up. All its contents are proprietary
 * and under german copyright law. Consider this file as closed source and/or
 * without the permission to reuse or modify its contents.
 * This license is available through the world-wide-web at the following URI:
 * https://rmp-up.de/license-generic.txt . If you did not receive a copy
 * of the license and are unable to obtain it through the web, please send a
 * note to mail@rmp-up.de so we can mail you a copy.
 *
 * @package   wp-di
 * @copyright 2021 Pretzlaw
 * @license   https://rmp-up.de/license-generic.txt
 */

declare( strict_types=1 );

namespace RmpUp\WpDi\Test\Primitives\YamlExpressions;

use RmpUp\WpDi\Test\AbstractTestCase;
use RmpUp\WpDi\Test\Mirror;
use SomeThing;

/**
 * Superglobals
 *
 * For injecting superglobals like `$GLOBALS` or `$_POST`
 * you can use a Tag/Command like the following:
 *
 * ```yaml
 * services:
 *   SomeThing:
 *     arguments:
 *       - !global [ wpdb ]
 *       - !server [ REQUEST_URI ]
 *       - !get [ ID ]
 *
 *       # Same as $_SESSION['Daedalus']
 *       - !session [ Daedalus ]
 *
 *       # Same as $_POST['Vulkan']['Officer']
 *       - !post [ Vulkan, Officer ]
 *
 *       # All of $_ENV
 *       - !env []
 * ```
 *
 * @copyright 2021 Pretzlaw (https://rmp-up.de)
 */
class SuperglobalsTest extends AbstractTestCase
{
	/**
	 * @var array
	 */
	private $injectedVariables;

	/**
	 * @var Mirror
	 */
	private $serviceWithSuperglobals;

	protected function compatSetUp()
	{
		parent::compatSetUp();

		$_SERVER['REQUEST_URI'] = '/' . uniqid( '', true );
		$_SESSION['Daedalus'] = uniqid( '', true );

		$_POST = [ 'Vulkan' => [ 'Officer' => "T'Pol" ] ];
		$_GET['ID'] = "Kir'Shara";

		$this->registerServices();

		$this->serviceWithSuperglobals = $this->pimple[ SomeThing::class ];
		$this->injectedVariables = array_combine(
			[
				'global',
				'server',
				'get',
				'session',
				'post',
				'env',
			],
			$this->serviceWithSuperglobals->getConstructorArgs()
		);
	}

	/**
	 * @return array|false|string
	 */
	private function getEnv()
	{
		if ( 7 === PHP_MAJOR_VERSION && 0 === PHP_MINOR_VERSION ) {
			return $_ENV;
		}

		return getenv();
	}

	public function testEnv()
	{
		static::assertSame( $this->getEnv(), $this->injectedVariables['env'] );
	}

	public function testGet()
	{
		static::assertSame( "Kir'Shara", $this->injectedVariables['get'] );
	}

	public function testGlobal()
	{
		global $wpdb;

		static::assertSame( $wpdb, $this->injectedVariables['global'] );
	}

	public function testPost()
	{
		static::assertSame( "T'Pol", $this->injectedVariables['post'] );
	}

	public function testServer()
	{
		static::assertSame( $_SERVER['REQUEST_URI'], $this->injectedVariables['server'] );
	}

	public function testSession()
	{
		static::assertSame( $_SESSION['Daedalus'], $this->injectedVariables['session'] );
	}
}
